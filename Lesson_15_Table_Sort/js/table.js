console.log('App was loaded...');

const addHandle = () => {
	document.getElementById('id').addEventListener('click', handleIdClick);
	document.getElementById('salary').addEventListener('click', handleSalaryClick);
	document.getElementById('position').addEventListener('click', handlePositionClick);
	document.getElementById('tech').addEventListener('click', handleTechClick);
	document.getElementById('full-name').addEventListener('click', handleFullNameClick);
	document.getElementById('exp').addEventListener('click', handleExpClick)
	document.getElementById('sex').addEventListener('click', handleSexClick);
}
////////////////////////////////////////////////////
const id = document.getElementById('id');
let salary = document.getElementById('salary');
let position = document.getElementById('position');
let fullName = document.getElementById('full-name');
let tech = document.getElementById('tech');
let sex = document.getElementById('sex');
const exp = document.getElementById('exp');
////////////////////////////////////////////////////
const handleIdClick = (e) => {
	let flag = id.dataset.flag == 'true' ? false : true;
	id.dataset.flag = flag;
    const sortStuff = stuff.sort((a, b) => (flag) ? 
    		a.id - b.id :
    		b.id - a.id);
    id.removeEventListener('click', handleIdClick)
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == id.id && id.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == id.id && id.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } )
    addHandle();
}
///////////////////////////////////////////////////////
const handleSalaryClick = (e) => {
	let flag = salary.dataset.flag == 'true' ? false : true;
	salary.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)?
    	a.salary - b.salary:
    	b.salary - a.salary);
    salary.removeEventListener('click', handleSalaryClick);
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == salary.id && salary.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == salary.id && salary.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } );
    addHandle();
}
///////////////////////////////////////////////////////
const handlePositionClick = (e) => {
	let flag = position.dataset.flag == 'true' ? false : true;
	position.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)?
    	a.position.toLowerCase().localeCompare(b.position.toLowerCase()):
    	b.position.toLowerCase().localeCompare(a.position.toLowerCase()));
    position.removeEventListener('click', handlePositionClick);
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == position.id && position.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == position.id && position.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } );
    addHandle();
}
///////////////////////////////////////////////////////
const handleTechClick = (e) => {	
	let flag = tech.dataset.flag == 'true' ? false : true;
	tech.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)? 
    	 a.skill.toLowerCase().localeCompare(b.skill.toLowerCase()):
    	 b.skill.toLowerCase().localeCompare(a.skill.toLowerCase()));
    tech.removeEventListener('click', handleTechClick);
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == tech.id && tech.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == tech.id && tech.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } );
    addHandle();
}
///////////////////////////////////////////////////////
const handleFullNameClick = (e) => {
	let flag = fullName.dataset.flag == 'true' ? false : true;
	fullName.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)? 
    	 a.fullName.toLowerCase().localeCompare(b.fullName.toLowerCase()):
    	 b.fullName.toLowerCase().localeCompare(a.fullName.toLowerCase()));
    fullName.removeEventListener('click', handleFullNameClick);
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == fullName.id && fullName.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == fullName.id && fullName.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } );
    addHandle();
}
////////////////////////////////////////////////////
const handleExpClick = () => {
	let flag = exp.dataset.flag == 'true' ? false : true;
	exp.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)?a.exp - b.exp:b.exp - a.exp);
    exp.removeEventListener('click', handleExpClick)
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == exp.id && exp.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == exp.id && exp.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } );
    addHandle();
}
///////////////////////////////////////////////////////
const handleSexClick = (e) => {
	let flag = sex.dataset.flag == 'true' ? false : true;
	sex.dataset.flag = flag;
    const sortStuff = stuff.sort((a,b) => (flag)? 
    	 a.sex.toLowerCase().localeCompare(b.sex.toLowerCase()):
    	 b.sex.toLowerCase().localeCompare(a.sex.toLowerCase()));
    sex.removeEventListener('click', handleSexClick);
    const thCollection = render(columns, sortStuff);
    [...thCollection].map((el, ind) => {
    	const text = el.innerText;
    	const arrowUp = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-up']}], null);
    	const arrowDown = createHTMLNode('i', [{name: 'class', value: ['fas', 'fa-arrow-down']}], null);
    	el.id == sex.id && sex.dataset.flag == 'true' ?
    		(el.replaceChild(arrowDown, el.children[0])) :
    		el.id == sex.id && sex.dataset.flag == 'false' ? 
    		(el.replaceChild(arrowUp, el.children[0])) :
    		null;
    } )
    addHandle();
}
///////////////////////////////////////////////////////
id.addEventListener('click', handleIdClick);
salary.addEventListener('click', handleSalaryClick);
position.addEventListener('click', handlePositionClick);
fullName.addEventListener('click', handleFullNameClick);
tech.addEventListener('click', handleTechClick);
sex.addEventListener('click', handleSexClick);
exp.addEventListener('click', handleExpClick);