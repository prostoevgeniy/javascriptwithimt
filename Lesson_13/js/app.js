console.log('lesson 13 is here ...');

// axios.get('https://rickandmortyapi.com/api/character/',)
fetch('https://rickandmortyapi.com/api/character/')
    .then(res => res.json())
    .then(res => {
        const {pages} = res.info
        Promise.all([... new Array(pages)].map((el, ind) => fetch(`https://rickandmortyapi.com/api/character/?page=${ind+1}`).then(res => res.json())))
            .then(res => {
                const arr = [];
                res.forEach((el, ind) => {

                    el.results.forEach((elem, ind) => {
                    	arr.push(elem);
                    //	localStorage.setItem(`${elem.id}`, JSON.stringify(elem));
                    })           
                } )
                const arr20 = arr.filter((el, ind) => ind < 20 );
                const heroWrapper = () => {
			    	const hero__SubTitleWrapper = createHTMLNode('div', [{name:'class', value:['hero__SubTitleWrapper']}], [
        				createHTMLNode('h2',[{name:'class', value:['hero__SubTitle']}],'Hey, did you ever want to hold a Terry fold?'),
        				createHTMLNode('h2',[{name:'class', value:['hero__HiddenSubTitle']}],'&nbsp I got one right here, grab my terry flap &nbsp'),
    				])
    				const h1 = createHTMLNode('h1', [{name:'class', value:['hero__Title']}],'The Rick and Morty API')
    				return createHTMLNode('section',[{name:'class', value:['hero__Wrapper']}],[h1,hero__SubTitleWrapper])
				}

				const getCharacterCard = ({image,name,id,status,species,gender,origin,location}) => {
    				const cardHeader = createHTMLNode('div',[{name:'class', value:['card','header']}], [
        				createHTMLNode('div',[{name:'class', value:['card-image']}], [
            				createHTMLNode('img',[{name:'src', value:[image]}],null)
        				]),
        				createHTMLNode('div',[{name:'class', value:['characterCard__Title']}], [
            				createHTMLNode('h2',[{name:'class', value:['characterCard__Name']}],name),
            				createHTMLNode('p',[{name:'class', value:['characterCard__Description']}],`id: ${id} - created 2 years ago`),
        				]),
    				])

    				const cardInfo = createHTMLNode('div',[{name:'class', value:['card','info']}],[{k:'STATUS',v:status},{k:'SPECIES',v:species},{k:'GENDER',v:gender},{k:'ORIGIN',v:origin.name},{k:'LAST LOCATION',v:location.name}].map(el => {
        				return createHTMLNode('div',[{name:'class', value:['characterCard__TextWrapper']}],[
            				createHTMLNode('span',[],el.k),
            				createHTMLNode('p',[],el.v),
        				])
    				}));

    				return createHTMLNode('div',[{name:'class', value:['characterCard__Wrapper']}], [cardHeader,cardInfo])
				}

				const chartersWrapper = heros => {
    				return createHTMLNode('section',[{name:'class', value:['showcase__Wrapper']}], [
        					createHTMLNode('div',[{name:'class', value:['showcase__Inner']}], heros.map(hero => getCharacterCard(hero)))
    					])	
				}
				renderInApp([heroWrapper(), chartersWrapper(arr20)]);
            })
    	})
