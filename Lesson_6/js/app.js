console.log('Lesson 6 is here...');

/*	
// 1 Part
getArrayOfRandomObjects(10);
// Возвращает массив объектов с полями fulName, salary значения которых 
// генерируются случайным образом.
[
    {fullName:'Oleh Lev', salary:1000},
]

// 2 Part

sortStuffBySalary([], 'asc|desc')
*/

const firstName = {
	'0': 'Oleh', '1': 'Libeta', '2': 'Liseta', '3': 'Myuseta', '4': 'Joneta', '5': 'Jorjeta',
	'6': 'Pedro', '7': 'Alex'
};

const lastName = {
	'0': 'Lev', '1': 'Lopes', '2': 'Rodriguas', '3': 'Pereyra', '4': 'Quelho',
	'5': 'Sanches', '6': 'Soares', '7': 'Sousa'
};

const salary = {
	'0': '1000', '1': '1500', '2': '2000', '3': '2500', '4': '3000',
	'5': '3500', '6': '4000', '7': '5000'
}
///////////////////      Part 1      ///////////////////////////////////
////////////////////////////////////////////////////////////////////////
function getArrayOfRandomNumber (n) {
	let arr = [];

	for (let i=0; i<n; i++ ) {
		arr.push(Math.floor(Math.random() * 8));
	}
	return arr;
}
////////////////////////////////////////////////////////////////////////
function getFulNameOrSalary(firstN, lastN = null){

	if ( ! lastN ) {
		return firstN[getArrayOfRandomNumber(1)[0].toString()];
	}
	else{
	let fulName = `${firstN[getArrayOfRandomNumber(1)[0].toString()]} ${lastN[getArrayOfRandomNumber(1)[0].toString()]}`;
	return fulName;
	}
}
////////////////////////////////////////////////////////////////////////
function getRandomName(obj){
	let propertyName = getArrayOfRandomNumber(1)[0].toString();
	let res = obj[propertyName];
	return res;
}
////////////////////////////////////////////////////////////////////////
function getArrayOfRandomObjects(n) {
	let arr = getArrayOfRandomNumber(n);
	arr = arr.map((elem, ind) => {
		return {
			fullName: getFulNameOrSalary(firstName, lastName),
			salary: getFulNameOrSalary(salary)
		}
	})

	return arr;
}

console.log(getFulNameOrSalary(firstName, lastName));
console.log(getFulNameOrSalary(salary));
console.log('//////////////////////////////////////////////');
console.log(getArrayOfRandomObjects(10));
////////////////////////////////////////////////////////////////////////
///////////////////      end Part 1      ///////////////////////////////
///////////////////      Part 2      ///////////////////////////////////
// sortStuffBySalary([], 'asc|desc')
function sortStuffBySalary(arr, param2 = 'asc') {
	
	switch (param2) {
		case 'asc':
		arr.sort((a, b) => parseInt(a.salary) - parseInt(b.salary));
		console.log('asc', arr);
		break;
		case 'desc':
		arr.sort((a, b) => parseInt(b.salary) - parseInt(a.salary) );
		break;
		default:
		console.log('second argument is incorrect');
	}
	return arr;
}
console.log('//////////////////      salary sorting      /////////////////////');

let newObjArr = getArrayOfRandomObjects(20);

console.log(newObjArr);
console.log(sortStuffBySalary(newObjArr, 'desc'));
/////////////////////////////      end Part 2      ///////////////////////////////////////////

