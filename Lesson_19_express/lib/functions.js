

const functions = {	
	checkElementNumberOrNot : (arr) => {
		const newArr = arr.filter((el) => {
			//return Number.isInteger(+el);
			return parseFloat(+el);
		});
		return newArr;
	},
	////////////////////////////////////////////////////////
	createHTMLNodeSimple : (tag, attrs, inner) => {
	    const element = document.createElement(tag);
	    attrs.map(attr => {element.setAttribute(attr.name, attr.value.join(' '))});
	    inner?element.innerHTML=inner:null;
	    return element;
	},

	////////////////////////////////////////////////////////
	createHTMLNode : (tag, attrs, inner) => {
	    const element = document.createElement(tag);
	    attrs.map(attr => {element.setAttribute(attr.name, attr.value.join(' '))});
	    inner ?  Array.isArray(inner) ? inner.map(el => element.appendChild(el)):
	             element.innerHTML=inner :
	             null;
	    return element;
	},

	/////////////////////////////////////////////////////////////////////
	findMinMaxToralAvg : (arr) => {
		const obj = arr.reduce((accum, el, ind) => {
			accum = ind == 0 ?  {min: el, max: el, total: el, avg: el} :
			ind == 1 ?  {min:  Math.min.call(null, accum.min, el), max: Math.max.call(null, accum.max, el), total: accum.total + el, avg: accum.avg + el} :
			ind < arr.length -1 ? {min: Math.min.call(null, accum.min, el), max: Math.max.call(null, accum.max, el), total: accum.total + el, avg: accum.avg + el} :
			ind == arr.length - 1 ? {min:  Math.min.call(null, accum.min, el), max: Math.max.call(null, accum.max, el),
			total: accum.total + el, avg: (accum.avg + el)/arr.length} : null;		
			return accum;
		}, 0);
		return obj;
	},

	////////////////////////////////////////////////////////
	renderInApp : htmlNode => {
	    document.getElementById('app').innerHTML = ''
	    htmlNode.map(el => document.getElementById('app').appendChild(el));
	},

	//////////////////////////////////////////////////
	compareStrings : (str, str2) => {
		return str.localeCompare(str2);
	},

	slug : (text) => {
		return text.trim().split(' ').map(el => el.toLowerCase()).join('-');
	}
}
