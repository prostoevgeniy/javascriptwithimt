const express = require('express');
const app = express();

const columns = ['ID', 'Full Name', 'Position', 'Tech', 'Exp', 'Sex', 'Salary'];

const stuff = [
    { id: 1, fullName: 'Oleh Lev', position: 'Web Dev', skill: 'PHP,JS', exp: 3, sex: 'Male', salary: 4500 },
    { id: 2, fullName: 'John White', position: 'Web Dev', skill: 'PHP', exp: 1, sex: 'Male', salary: 1200 },
    { id: 3, fullName: 'Jany Rad', position: 'Sale', skill: '-', exp: 2, sex: 'Famale', salary: 3500 },
    { id: 4, fullName: 'Ivan Brown', position: 'iOS', skill: 'Swift', exp: 3, sex: 'Male', salary: 4000 },
    { id: 5, fullName: 'Pet Bool', position: 'Android', skill: 'Java', exp: 2, sex: 'Male', salary: 3520 },
    { id: 6, fullName: 'Emma Hallo', position: 'Android', skill: 'Kotlin', exp: 1, sex: 'Famale', salary: 2520 },
    { id: 7, fullName: 'Olivia Jones', position: 'iOS', skill: 'Objective-C', exp: 3, sex: 'Famale', salary: 2820 },
    { id: 8, fullName: 'William Smith', position: 'Designer', skill: '-', exp: 5, sex: 'Male', salary: 3000 },
    { id: 9, fullName: 'Oliver Alien', position: 'PM', skill: '-', exp: 4, sex: 'Male', salary: 6000 },
    { id: 10, fullName: 'Mia Morris', position: 'Owner', skill: '-', exp: 10, sex: 'Famale', salary: 10000 },
    { id: 11, fullName: 'Evetta Pereira', position: 'Web Dev', skill: 'JS', exp: 3, sex: 'Famale', salary: 4300 },
    { id: 12, fullName: 'Miusetta Miranda', position: 'Designer', skill: '-', exp: 1, sex: 'Famale', salary: 3200 },
    { id: 13, fullName: 'Jonetta Pinto', position: 'Sale', skill: '-', exp: 2, sex: 'Famale', salary: 3500 },
    { id: 14, fullName: 'Jorgetta Bella', position: 'iOS', skill: 'Swift', exp: 3, sex: 'Famale', salary: 4000 },
    { id: 15, fullName: 'Koletta Peres', position: 'Android', skill: 'Java', exp: 2, sex: 'Famale', salary: 3520 },
    { id: 16, fullName: 'Poletta Riviera', position: 'Android', skill: 'Kotlin', exp: 1, sex: 'Famale', salary: 2520 },
    { id: 17, fullName: 'Kloretta Dias', position: 'iOS', skill: 'Objective-C', exp: 3, sex: 'Famale', salary: 2820 },
    { id: 18, fullName: 'Floretta Maceira', position: 'Designer', skill: '-', exp: 5, sex: 'Famale', salary: 3000 },
    { id: 19, fullName: 'Marietta Alien', position: 'PM', skill: '-', exp: 4, sex: 'Famale', salary: 6000 },
    { id: 20, fullName: 'Klara Luisa Morris', position: 'Owner', skill: '-', exp: 10, sex: 'Famale', salary: 10000 },
];

app.get('/api', (req, res) => {
    const perPage = 'perPage' in req.query && Number(req.query.perPage) ? Number(req.query.perPage) : 3;
    const currentPage = 'page' in req.query && Number(req.query.page)? Number(req.query.page) : 1;
    const urlPrefix = '/api?page=';
    const pages = Math.ceil(stuff.length/perPage);
    res.header("Access-Control-Allow-Origin", "*");
    res.json({
        info: {
            count: stuff.length,
            pages,
            next: getNextPageUrl(currentPage, pages, urlPrefix),
            prev: getPrevPageUrl(currentPage, urlPrefix),
            currentPage,
            perPage,
          },
          results: stuff.slice((currentPage-1)*perPage,currentPage*perPage)
    })
});

const getNextPageUrl = (current_page, total_pages, url_prefix) => {
    return current_page < total_pages ? `${url_prefix}${current_page+1}` : null
}
const getPrevPageUrl = (current_page, url_prefix) => current_page > 1 ? `${url_prefix}${current_page-1}` : null

app.listen(3000, () => {
    console.log('Server was runnig...')
})