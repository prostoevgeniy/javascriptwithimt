console.log('request handler is here...');
const columns = ['ID', 'Full Name', 'Position', 'Tech', 'Exp', 'Sex', 'Salary'];
//////////////////////////////////////////////////////
const render = (columnData, rowData) => {
	const tBody = functions.createHTMLNode('tbody', [], null);

	rowData.map((el) => {
		const tr = functions.createHTMLNode('tr', [], null);
		Object.keys(el).map((keyName) => {
			const td = functions.createHTMLNode('td', [], el[keyName]);
			tr.appendChild(td);
		});

		tBody.appendChild(tr);
	});

	const tr = functions.createHTMLNode('tr', [], null);
	columnData.map((el) => {
		const th = functions.createHTMLNode('th', [{ name: 'id', value: [functions.slug(el)] }], `${el}` );
		tr.appendChild(th);
	});

	const tHead = functions.createHTMLNode('thead', [], null);
	tHead.appendChild(tr);

	const table = functions.createHTMLNode('tabl', [], null);
	table.appendChild(tHead);
	table.appendChild(tBody);

	return table;
}
//////////////////////////////////////////////////////

const submitBut = document.getElementById("submitButton");
submitBut.addEventListener('click', (e) => {
	e.preventDefault();
		
            let selectionForm = document.forms["dataExpress"];
            let pageNumber = selectionForm.elements["page"].value;
            let perPageNumber = selectionForm.elements["perPage"].value;
	let url = `http://localhost:3000/api/?page=${pageNumber}&perPage=${perPageNumber}`;
	fetch(url)
		.then(response => response.json())
		.then(response => {
			const result = response.results;
			document.getElementById('main').innerHTML = '';
			return document.getElementById('main').appendChild(render(columns, result));
		});
});
