# Node Express

Processing a response to a server request

1. For install node_modules type command:
```bash
npm install
```

2. For run server type command:
```bash
node app.js
```
3. Open the index.html file in the browser

4. Enter the numbers in the form input fields

5. the received response in the format JSON is processed in the file requestHandler.js
