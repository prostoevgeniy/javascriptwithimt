console.log("DZ Lesson 5 is here...");

/////////////      Homework      /////////////////////////////////
// 1.Написать алгоритм проверки кредит карт - Луна              //
// 4916 5526 5398 1949 - ОК                                     //
//                                                              //
// 2.Есть массив рандом чисел. Найти максимум и минимум.        //
//////////////////////////////////////////////////////////////////

/*
	Оригинальный алгоритм Луна, описанный разработчиком
1. Цифры проверяемой последовательности нумеруются справа налево.
2. Цифры, оказавшиеся на нечётных местах, остаются без изменений.
3. Цифры, стоящие на чётных местах, умножаются на 2.
4. Если в результате такого умножения возникает число больше 9, 
	оно заменяется суммой цифр получившегося произведения — 
	однозначным числом, то есть цифрой.
5. Все полученные в результате преобразования цифры складываются. 
	Если сумма кратна 10, то исходные данные верны. 
*/
let tempvar, flag = true, flagFor, result = null;
let numberOfCard ;

while(flag) {
	flagFor = true;
	tempvar =[];
	numberOfCard = prompt("Enter number of card. ( 16 digit ) ").trim();
	for(let i=0; i<numberOfCard.length; i++) {
		
		if ( Number.isNaN(+numberOfCard[i]) ) {
			flagFor = false;
			console.log(' Enter number again.');
			alert(' Enter number again.');
			break;
		}
		if (numberOfCard[i] != ' ' && typeof +numberOfCard[i] == 'number' ) {
			tempvar.push( + numberOfCard[i]);
			
		}
		
	}

	if( ! flagFor) {
		continue;
	}

	flag = false;
}
 console.log(tempvar);

 // 1. Цифры проверяемой последовательности нумеруются справа налево.

 if (tempvar.length == 16){
 	tempvar = tempvar.reverse();
 }
 console.log(tempvar);

 // 2. Цифры, оказавшиеся на нечётных местах, остаются без изменений.
 // 3. Цифры, стоящие на чётных местах, умножаются на 2.

 tempvar = tempvar.map((elem, ind) => {
 	elem = (ind % 2 != 0) ? elem *= 2 : elem;
 	return elem;
 });
console.log(tempvar);

// 4. Если в результате такого умножения возникает число больше 9, 
//	оно заменяется суммой цифр получившегося произведения — 
//	однозначным числом, то есть цифрой.

function doubleDigitConversion(el) {
	let temp;
	temp = el % 10;
	el = parseInt(el/10);
	el = el + temp;
	return el;
}

tempvar = tempvar.map((elem, ind) => {
	let temp;
	elem = (ind % 2 != 0) ? 
		(elem > 9) ? elem = doubleDigitConversion(elem) : elem  : elem;
	return elem;
});

console.log(tempvar);

// 5. Все полученные в результате преобразования цифры складываются. 
//	Если сумма кратна 10, то исходные данные верны. 
result = tempvar.reduce((accum, currentv) => {
	return accum + currentv;
});

console.log(result);

result = (result % 10 == 0) ? `Checking the card number using the 'Lun' 
algorithm was successful. Card number ( ${numberOfCard} ) is valid.` : `Checking the card number 
using the 'Lun' algorithm revealed an error. Card number ( ${numberOfCard} ) not valid`;
console.log(result);
alert(result);

/////////////////////////////////////////////////////////////////////////////
// 2.Есть массив рандом чисел. Найти максимум и минимум.

let arr2 = [], maxValue = 0;

for (let i=0; i<10; i++ ) {
	arr2.push(Math.round(Math.random() * 100));
}

console.log(arr2);

arr2.forEach( elem => {
	(elem > maxValue) ? maxValue = elem : maxValue = maxValue;
});

console.log(maxValue);