console.log('App1 was loaded...');

/*
1. убрать пробелы из поля ввода пароля:
'  sdfgasd dfgdftg     ' => 'sdfgasddfgdftg'

2. Иконка скрытия пароля - при нажатии, менять иконку и показывать пароль!
Верстка - иконка должнабыть внутри инпута с правом углу.

3. Проверить email + password пользователя, вывести сообщение(Успех или ошибка)
*/
String.prototype.removingSpacesInString = function(){
	return Array.prototype.filter.call(this, (el) => el != ' ').join('');
} 
const str = '  sdfgasd dfgdftg     ';
const strResult = str.removingSpacesInString();
console.log(strResult === 'sdfgasddfgdftg');
console.log(strResult);
/////////////////////////////////////////////////////////////

const seePass = (ev) => {

	let eye = document.getElementById('eye');
	let eye2 = document.getElementById('eye2');
	let res = document.getElementById('exampleInputPassword1');

	if(res.type == 'password') {
		res.type = 'text';
		eye.classList.toggle('hideEye');
		eye2.classList.toggle('hideEye');
	} else {
		res.type ='password';
		eye.classList.toggle('hideEye');
		eye2.classList.toggle('hideEye');
	}
	
}
const eye = document.getElementById('eye');
const eye2 = document.getElementById('eye2');
eye.addEventListener('click', seePass);
eye2.addEventListener('click', seePass);
//////////////////////////////////////////////////////


const exampleInputEmail1 = document.getElementById('exampleInputEmail1');
const exampleInputPassword1 = document.getElementById('exampleInputPassword1');

const isPasswordValid = password => password.length < 6;
const isEmailValid = email => !email.split('').filter(el=>el === '@').length;

exampleInputEmail1.addEventListener('input', e => {

    if(isEmailValid(e.target.value)){
        // Error
        e.target.classList.add('is-invalid')
        e.target.classList.remove('is-valid')
    }else{
        e.target.classList.remove('is-invalid')
        e.target.classList.add('is-valid')
    }
})

exampleInputPassword1.addEventListener('input', e => {
    if(isPasswordValid(e.target.value)){
        e.target.classList.add('is-invalid')
        e.target.classList.remove('is-valid')
    }else{
        e.target.classList.remove('is-invalid')
        e.target.classList.add('is-valid')
    }
})

loginForm.addEventListener('submit', e => {
    e.preventDefault();
    if(document.getElementById('col').lastElementChild.classList.contains('alert')) {
    	let elem = document.getElementById('col').lastElementChild;
    	elem.parentNode.removeChild(elem);
    }

     const validLog = [{email: 'olevchenko@gmail.com', password: '11111111'}, 'test@gmail.com'];

    if (e.target[0].value == validLog[0].email && e.target[1].value == validLog[0].password){
    	 const alertDiv = createHTMLNodeSimple('div', [{name: 'class', value: ['alert', 'alert-success']}], 'Form submitted in good faith');
    	 document.getElementById('col').appendChild(alertDiv);
    }
   	else {
   		 const alertDiv = createHTMLNodeSimple('div', [{name: 'class', value: ['alert', 'alert-danger']}], 'Login or password does not match the original');
   		 document.getElementById('col').appendChild(alertDiv);
   	}

})
