console.log('Lesson 9 is here...');

/*
	Rick and Morty API - Analize!!! Count... Filter...
Create function for random product... For rozetka

{
    name: 'name',
    desc: 'desc',
    price: 100,
    available: true,
    cat: 'Category',
    amount: 12,
}

Create func add to cart into LocalStorage

addProductToCart(1)
addProductToCart(product)
*/
let flag = true,  tempData, strDataName;
const data = {
	name: ["Ноутбук Asus", 'Мобильный телефон Samsung', 'Реле напряжения ZUBR D63', 'Комплект белья LightHouse',
			'Видеорегистратор Xiaomi Yi', 'С/д лампа P45 5W 3000K E14 220V', 'Грядка Контур Рент'],
	cat: ['Ноутбуки и компьютеры', 'Смартфоны, ТВ и Электроника', 'Бытовая техника', 'Товары для дома', 'Инструменты и автотовары', 
		 'Сантехника и ремонт', 'Дача, сад и огород', 'Спорт и увлечения', 'Одежда, обувь и украшения', 'Красота и здоровье', 
		 'Детские товары', 'Канцтовары и книги', 'Алкогольные напитки и продукты', 'Товары для бизнеса', 'Лучшее за год', 'Туры и отдых'],
	products:[
		{
			name: 'Ноутбук Asus',
		    desc: 'Экран 15.6" IPS (1920x1080) Full HD, матовый / Intel Core i7-9750H (2.6 - 4.5 ГГц) / RAM 8 ГБ / SSD 512 ГБ / nVidia GeForce GTX 1650, 4 ГБ / без ОД / LAN / Wi-Fi / Bluetooth / без ОС / 2.4 кг / черный',
		    price: 25999,
		    available: true,
		    cat: 'Ноутбуки и компьютерыCategory',
		    amount: 2,
		},
		{
			name: 'Мобильный телефон Samsung',
		    desc: 'Экран (6.4", Super AMOLED, 2340х1080) / Samsung Exynos 9611 (4 x 2.3 ГГц + 4 x 1.7 ГГц) / тройная основная камера: 48 Мп + 8 Мп + 5 Мп, фронтальная 16 Мп / RAM 4 ГБ / 64 ГБ встроенной памяти + microSD (до 512 ГБ) / 3G / LTE / GPS / ГЛОНАСС / BDS / Galileo / поддержка 2х SIM-карт (Nano-SIM) / Android 9.0 (Pie) / 6000 мА*ч',
		    price: 5555,
		    available: true,
		    cat: 'Смартфоны, ТВ и Электроника',
		    amount: 6,
		},
		{
			name: 'Реле напряжения ZUBR D63',
		    desc: 'Мощность нагрузки: 13900 ВА. Ток нагрузки: 63 А. Напряжение питания: 100–400 В. Верхний предел напряжения: 220–280 В. Нижний предел напряжения: 120–210 В. Габариты: 7×8,5×5,3 ',
		    price: 776,
		    available: true,
		    cat: 'Бытовая техника, интерьер',
		    amount: 20,
		},
		{
			name: 'Комплект белья LightHouse',
		    desc: 'Пододеяльник: 200x220 см - 1 шт Простынь: 220x240 см - 1 шт Наволочка: 50x70 см - 2 шт',
		    price: 5555,
		    available: true,
		    cat: 'Товары для дома',
		    amount: 10,
		},
		{
			name: 'Видеорегистратор Xiaomi Yi',
		    desc: 'Максимальное разрешение видео SuperHD (2304x1296) Количество камер 1 Встроенный GPS Нет Запись звука Yes Страна регистрации бренда Китай',
		    price: 1299,
		    available: true,
		    cat: 'Инструменты и автотовары',
		    amount: 5,
		},
		{
			name: 'С/д лампа P45 5W 3000K E14 220V',
		    desc: 'Тип цоколя E14 Световой поток 450 Лм Эквивалент мощности лампы накаливания 40 Вт',
		    price: 19,
		    available: true,
		    cat: 'Сантехника и ремонт',
		    amount: 200,
		},
		{
			name: 'Грядка Контур Рент',
		    desc: 'Уникальная металлическая грядка Контур Рент, данная грядка создана для садоводов любителей, которые заботятся о красоте своего сада или огорода. Для тех, Кто устал от повседневности и хочет, что бы огород и сад радовали не только садовыми культурами, но и эстетичным внешним видом за небольшие затраты. Мы предлагаем необычное решение: металлическую грядку.',
		    price: 464,
		    available: true,
		    cat: 'Дача, сад и огород',
		    amount: 100,
		},
	]
}
///////////////////////////////////////////////////////////
function addRandomProduct(dat){
	let name = '', description = '', flag = true, obj = {}, arr = [];
	let category = dat.cat[Math.floor(Math.random()*dat.cat.length)];
	let price = Math.floor(Math.random()*1000*Math.random());
	let available = !!Math.floor(Math.random()*2);
	let amount = Math.floor(Math.random()*100*Math.random());
	while(flag) {		
		name = prompt(`Введите название товара для категории \n"${category}"`);
		name ? description = prompt(`Введите описание товара "${name}"`) :
		alert('Вы отменили ввод названия товара.');
		if( ! Number.isNaN(+name) || name == '' || name == ' ') {
			flag = confirm('Хотите попробовать ввести название товара ещё раз?');
			continue;
		}
		 if ( ! Number.isNaN(+description) || description == '' || description == ' ') {
		 	alert('Введите название товара, а затем его описание ещё раз.');
			continue;
		}
		flag = false;
	}
	obj.name = name;
	obj.desc = description;
	obj.price = price;
	obj.available = available;
	obj.cat = category;
	obj.amount = amount;

	dat.name.push(name);
	dat.products.push(obj);

	console.log(dat.name);
	console.log(dat.products);
	return function() { 
		return dat;
	 };
}

/////////////////////////////////////////////////////////////////////
function addProductToCart(prod) {
	let temp;
	switch(true) {
		case (!Number.isNaN(+prod)):
			if(typeof tempData == 'function' && tempData().products.length >= +prod && +prod > 0){
				let tempobj = Object.assign({}, tempData().products[prod - 1]);
				temp = JSON.stringify(tempobj);
			localStorage.setItem(tempData().products[prod - 1].name, temp);
			} else if(data.products.length >= +prod && +prod > 0) {
			let tempobj = Object.assign({}, data.products[prod - 1]);
			temp = JSON.stringify(tempobj);
			localStorage.setItem(data.products[prod - 1].name, temp);
		} else {
			alert(`Продукт не найден ...`);
		}
		break;
		case (Number.isNaN(+prod)):
			if(typeof tempData == 'function' ) {
				console.log('name prod string');
				let tempobj = Object.assign({}, tempData().products.filter((el, ind) => { return el.name == prod; }));
				temp = JSON.stringify(tempobj[0]);
				temp.length ? localStorage.setItem(prod, temp) :
				alert(`Продукт не найден .`);
			} else {
				let tempobj = Object.assign({}, data.products.filter((el, ind) => { return el.name == prod; }));
				temp = JSON.stringify(tempobj[0]);
				temp.length ? localStorage.setItem(prod, temp) :
				alert(`Продукт не найден .`);
			}			
		break;
		default:
			alert('Соответствие не найдено');
		break;
	}
	return temp;
}
///////////////////////////////////////////////////////////

while(flag) {
	let temp;
	strDataName = data.name.join("<|\n|>");
	while(flag){
		flag = confirm("Хотите добавить новый товар ?");
		if(flag) {
			tempData = addRandomProduct(data);
			console.log(typeof tempData);
			strDataName = tempData().name.join("<|\n|>");
		} else {
			break;
		}		
	}
	flag = confirm("Хотите добавить товар в корзину ? (localStorage)");
	while(flag) {
		temp = flag ? prompt(`Введите число или выберите название продукта  \n\t|>${strDataName}`) : false;
		addProductToCart(temp);
		flag = confirm("Хотите продолжить ?");
	}
}