console.log('App was loaded...');

const a = document.querySelectorAll('ul.list-unstyled.main a');
const ul = document.querySelectorAll('ul.list-unstyled.main ul');
////////////////////////////////////////////////////////
const addClassParentUl = (source, className) => {
	const parent = source.parentElement;
	if(parent.parentElement.classList.contains('main')) {
		return ;
	} else {
		parent.classList.add(className);
		addClassParentUl(parent, className);
	}
}
/////////////////////////////////////////////////////////
[].map.call(a, (el) => el.addEventListener('click', (e) => {
  e.preventDefault();
  [...a].map(el => el.classList.remove('selected'));
  [...ul].map(el => el.classList.remove('open'));
  if(e.target.previousElementSibling){
    e.target.previousElementSibling.classList.add('open')
  }
  e.target.classList.add('selected');
  addClassParentUl(e.target, 'open');  
}));