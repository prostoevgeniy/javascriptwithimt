console.log('Lesson 11 is here...');

// const tdElems = document.getElementsByTagName('td');
// 

const elemsTd = document.querySelectorAll('tr td:nth-child(7n + 7)');
const tr = document.createElement('tr');
const tr2 = document.createElement('tr');
const td = document.createElement('td');

console.log(elemsTd);

let arr = Array.from(elemsTd, (el => +el.innerHTML));
console.log(arr);

let res = arr.slice(0, -2).reduce((accum, el) => {
	return accum + el;
}, 0);

console.log(res);
console.log(res / 12);

elemsTd[12].innerHTML = String(res);
elemsTd[13].innerHTML = String(res / 12);
