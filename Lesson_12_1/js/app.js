console.log('Lesson 12 is here...');

/*
1. пользователь вводит строку, например
"11,58,69,edfygd,5798,gh,44,66,gdfg,25"

max
min
total
avg
*/

const str = prompt('Введите строку подобную следующей "11,58,69,edfygd,5798,gh,44,66,gdfg,25"');
const strArray = str.split(',');
console.log(strArray);
////////////////////////////////////////////////////////
const checkElementNumberOrNot = (arr) => {
	const newArr = arr.filter((el) => {
		//return Number.isInteger(+el);
		return parseFloat(+el);
	});
	return newArr;
}
////////////////////////////////////////////////////////
const createHTMLNode = (tag, attrs, inner) => {
    const element = document.createElement(tag);
    attrs.map(attr => {element.setAttribute(attr.name, attr.value.join(' '))});
    inner?element.innerHTML=inner:null;
    return element;
}

////////////////////////////////////////////////////////
let resArr = checkElementNumberOrNot(strArray).map(el => +el);
console.log(resArr);
////////////////////////////////////////////////////////
const findMinMaxToralAvg = (arr) => {
	const obj = arr.reduce((accum, el, ind, arr) => {
		accum = ind == 0 ?  {min: el, max: el, total: el, avg: el} :
		
		ind < arr.length -1 ? {min: Math.min.call(null, accum.min, el), max: Math.max.call(null, accum.max, el), total: accum.total + el, avg: accum.avg + el} :
		ind == arr.length - 1 ? {min:  Math.min.call(null, accum.min, el), max: Math.max.call(null, accum.max, el),
		total: accum.total + el, avg: (accum.avg + el)/arr.length} : null;		
		return accum;
	}, 0);
	return obj;
}

const newObj = findMinMaxToralAvg(resArr);
console.log(`Min value is "${newObj.min}", Max value is "${newObj.max}", Total value is "${newObj.total}", AVG value is "${newObj.avg}".`);
const outResultConteiner = document.getElementById('outResult');
const newElement = createHTMLNode('div', [], `Min value is "${newObj.min}", Max value is "${newObj.max}", Total value is "${newObj.total}", AVG value is "${newObj.avg}".`);
outResultConteiner.appendChild(newElement);
///////////////////////      1. end      ////////////////////////////////////