console.log('Lesson 10 is here...');

/*
	2 кубика 1-6
	Игрок - 1000 грн

	ставка 1-максимум

	пишет число 2-12

	выигрыш х2

	Игра идёт до 10 000 или проиграл

	Сколько ходов сделает игрок ?
*/

class GameOfCubes {
	constructor(){
		this.count;
		this.flag = true;
		this.value = 1000;
	 	this.resArray = [[],[]];
	}
	getRandomNumber = () => Math.ceil(Math.random()*6)

	checkResult = () => {
		switch(true){
			case (this.resArray[0][0] === this.resArray[0][1] && this.resArray[1][0] === this.resArray[1][1]) || 
			((this.resArray[0][0] + this.resArray[0][1] === this.resArray[1][0] + this.resArray[1][1]) && 
				(this.resArray[0][0] !== this.resArray[0][1] && this.resArray[1][0] !== this.resArray[1][1])) :
				alert('Ничия. Переиграйте.');
			break;
			case this.resArray[0][0] === this.resArray[0][1] && this.resArray[1][0] !== this.resArray[1][1] :
				this.value != 0 && this.value > 0 ? this.value -= 2*this.value : 
				this.value != 0 && this.value < 0 ? this.value += 2*this.value : 
				this.value -= 2000;
				this.count++;
				alert(`Игрок проиграл. Выигрыш игрока уменьшился вдвое и составляет ${this.value} грн`);
			break;
			case this.resArray[1][0] === this.resArray[1][1] && this.resArray[0][0] !== this.resArray[0][1] :
				this.value != 0 && this.value > 0 ? this.value += 2*this.value : 
				this.value != 0 && this.value < 0 ? this.value += (-2)*this.value :
				this.value += 2000 ;
				this.count++;
				alert(`Выигрыш игрока увеличивается вдвое и составляет ${this.value} грн`);
			break;
			case this.resArray[1][0] + this.resArray[1][1] > this.resArray[0][0] + this.resArray[0][1] :
				this.value += 1000;
				this.count++;
				alert(`Выигрыш игрока увеличивается на 1000 грн и составляет ${this.value}`);
			break;
			case this.resArray[1][0] + this.resArray[1][1] < this.resArray[0][0] + this.resArray[0][1] :
				this.value -= 1000;
				this.count++;
			alert(`Выигрыш игрока уменьшился на 1000 грн и составляет ${this.value}`);
			break;
		}
	}

	startGame = () => {
		while(this.flag){
			this.flag = confirm('Сыграем в кубики?');
			this.value = 1000, this.count = 0;
			while(this.flag){		
				this.resArray[0][0] = this.getRandomNumber();
				this.resArray[0][1] = this.getRandomNumber();
				alert(`Бросок ПК ${this.resArray[0][0]} : ${this.resArray[0][1]}`);
				this.resArray[1][0] = this.getRandomNumber();
				this.resArray[1][1] = this.getRandomNumber();
				alert(`Бросает игрок ${this.resArray[1][0]} : ${this.resArray[1][1]}`);
				this.checkResult()
				this.flag = (this.value <= -10000 || this.value >= 10000) ? false : true;
			}
			this.value >= 10000 && this.count != 0 ? alert(`Игра окончена. Победил игрок выполнив ${this.count} бросков. Выигрыш ${this.value}`) : 
			this.value <= 10000 && this.count != 0 ? alert(`Игра окончена. Игрок проиграл ${this.value}. Победили кубики выполнив ${this.count} бросков.`) : 
			alert(`Вы решили выйти из игры?`);
			this.flag = confirm('Сыграем в кубики ещё раз?');
			 console.log("Выигрыш ", this.value," Броски ", this.count );
		}
		return this;
	}
}
//////////////////////////////////////////////////////
const game = new GameOfCubes();
game.startGame();